#TP3- SISTEMAS DIGITALES 2 - FCEIA - Rosario -  

C project using Freescale KL46Z in MCUXpresso Ide. Communication using RS485 protocol

The project's pdf can be found [here](https://gitlab.com/luciano-buono/dig2_tp3_2019_fceia/blob/master/SD2_TP3_2019.pdf)

The project's [Master](https://i.imgur.com/cgNuFGw.png) can be found [here](https://gitlab.com/luciano-buono/dig2_tp3_2019_fceia/blob/master/setup_SD2+TP3+Master_win32.exe)
```
Implements: 
			* MMA8451Q
			* UART0
			* UART1-RS485 module
			* Ring buffer
```


If you encounter the problem:
```
Can't find a source file at "D:\Descargas\Facultad\DIgital II\2019\Projects_Eclipse\dig2_tp3_2019_fceia\Debug/../source/main.c" 
Locate the file or edit the source lookup path to include its location.
```
It can probably be resolved using  "Clean Project" option in the IDE


