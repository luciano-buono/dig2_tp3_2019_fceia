################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../source/Clock.c \
../source/SD2_I2C.c \
../source/SD2_board.c \
../source/key.c \
../source/main.c \
../source/mma8451.c \
../source/mtb.c \
../source/ringBuffer.c \
../source/semihost_hardfault.c \
../source/uart1_rs485.c \
../source/uart_ringBufer.c 

OBJS += \
./source/Clock.o \
./source/SD2_I2C.o \
./source/SD2_board.o \
./source/key.o \
./source/main.o \
./source/mma8451.o \
./source/mtb.o \
./source/ringBuffer.o \
./source/semihost_hardfault.o \
./source/uart1_rs485.o \
./source/uart_ringBufer.o 

C_DEPS += \
./source/Clock.d \
./source/SD2_I2C.d \
./source/SD2_board.d \
./source/key.d \
./source/main.d \
./source/mma8451.d \
./source/mtb.d \
./source/ringBuffer.d \
./source/semihost_hardfault.d \
./source/uart1_rs485.d \
./source/uart_ringBufer.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DSDK_OS_BAREMETAL -DFSL_RTOS_BM -DSDK_DEBUGCONSOLE=0 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -DCPU_MKL46Z256VLH4_cm0plus -DCPU_MKL46Z256VLH4 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I../board -I../source -I../ -I../drivers -I../CMSIS -I../utilities -I../startup -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -mcpu=cortex-m0plus -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


