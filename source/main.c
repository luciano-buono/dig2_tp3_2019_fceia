/* Copyright 2017, DSI FCEIA UNR - Sistemas Digitales 2
 *    DSI: http://www.dsi.fceia.unr.edu.ar/
 * Copyright 2017, Diego Alegrechi
 * Copyright 2017, Gustavo Muro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

/*New INCLUSIONS*/

// Standard C Included Files
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

// Project Included Files
#include "SD2_board.h"
#include "fsl_lpsci.h"
#include "fsl_port.h"
#include "board.h"
#include "MKL46Z4.h"
#include "pin_mux.h"

#include "Clock.h"
#include "uart_ringBuffer.h"
#include "mma8451.h"
#include "key.h"
#include "uart1_rs485.h"
#include "SD2_I2C.h"
/*-------END OF NEW INCLUSIONS--------------*/



#define NRO_GP_0  '0'
#define NRO_GP_1  '9'
#define TIMEDOWN_MS 8000
#define ENDOFTRANSFER 10

static bool validarDatos(uint8_t *pBuf, uint8_t size);
static void acciones (uint8_t *pBuf);
static void accionesLED (uint8_t *pBuf);
static void accionesSW (uint8_t *pBuf);
static void accionesAccel (uint8_t *pBuf);
void SysTick_Init(void);
void SysTick_Handler(void);

static uint8_t indexRx;
static int32_t timeDown1ms;
int16_t  Lectura_actual;
static void* pRingBufferRx;
static void* pRingBufferTx;

typedef enum
{
	EST_REC_DATOS_ESPERANDO_DOS_PUNTOS = 0,
	EST_REC_DATOS_GUARDANDO_DATOS ,
	EST_REC_DATOS_VALIDAR_RX,
	EST_REC_DATOS_ACCIONES
}estadoRecDatos_enum;






static void mefRecDatos(){
	static estadoRecDatos_enum estadoRecDatos = EST_REC_DATOS_ESPERANDO_DOS_PUNTOS;
	static uint8_t buffer[20];
	int32_t ret;
	uint8_t byterec;


	switch (estadoRecDatos){
		case EST_REC_DATOS_ESPERANDO_DOS_PUNTOS:
			ret= uart1_ringBuffer_recDatos(&byterec, 1);

			if ( ret != 0 && byterec == ':')
			{
				indexRx = 0;
				timeDown1ms= TIMEDOWN_MS;

				buffer[indexRx]= byterec;
				indexRx ++;
				estadoRecDatos = EST_REC_DATOS_GUARDANDO_DATOS;
			}

			break;

		case EST_REC_DATOS_GUARDANDO_DATOS:
			ret= uart1_ringBuffer_recDatos(&byterec, 1);
			if (ret != 0 && byterec != ':' && byterec != ENDOFTRANSFER)  //X=88   LF=10
			{
				buffer[indexRx]= byterec;
				indexRx ++;
				timeDown1ms=TIMEDOWN_MS;
			}


			if (ret != 0  && byterec == ENDOFTRANSFER) //10=LF    X=88
			{
				buffer[indexRx]= byterec;
				indexRx++;
				estadoRecDatos = EST_REC_DATOS_VALIDAR_RX;

			}

			if (ret != 0  && byterec == ':')
			{
				indexRx =0;
				timeDown1ms=TIMEDOWN_MS;
				buffer[indexRx]= byterec;
				indexRx ++;

			}
			if (timeDown1ms == 0)
			{
				estadoRecDatos = EST_REC_DATOS_ESPERANDO_DOS_PUNTOS;

			}
			if (indexRx >= 8)

			{
				estadoRecDatos = EST_REC_DATOS_ESPERANDO_DOS_PUNTOS;
			}

			break;

		case EST_REC_DATOS_VALIDAR_RX:
			//No envio aca, lo hago en cada accion

			if (validarDatos(buffer, indexRx))
				estadoRecDatos = EST_REC_DATOS_ACCIONES;
			else
				estadoRecDatos = EST_REC_DATOS_ESPERANDO_DOS_PUNTOS;

			break;


		case EST_REC_DATOS_ACCIONES:
			acciones (buffer);
			estadoRecDatos = EST_REC_DATOS_ESPERANDO_DOS_PUNTOS;
			break;

		default: estadoRecDatos = EST_REC_DATOS_ESPERANDO_DOS_PUNTOS;

	}
}


static bool validarDatos(uint8_t *pBuf, uint8_t size){
	bool ret= 1;

	if(size <6 || size >7)
		ret = 0;

	if (pBuf[1] != NRO_GP_0 || pBuf[2] != NRO_GP_1)
	{
		ret = 0;
	}


	if (pBuf[3] != '0' && pBuf[3] != '1' && pBuf[3] != '2')
	{
		ret = 0;
	}

	if (pBuf[4] < '0' || pBuf[4] > '9' )
	{
		ret = 0;
	}

	if (size ==7)
	{	if (pBuf[5] != 'E' && pBuf[5] != 'A' && pBuf[5] != 'T' )
			ret = 0;
	}
	return ret;
}


static void acciones (uint8_t *pBuf){

	if (pBuf[3] == '0'){
		accionesLED( pBuf);
	}

	if (pBuf[3] == '1'){
		accionesSW( pBuf);
	}

	if (pBuf[3] == '2'){
		accionesAccel(pBuf);
	}
}

static void accionesLED (uint8_t *pBuf){

	if (pBuf[4] == '1'){
		if (pBuf[5] == 'A')
			board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_OFF);
		if (pBuf[5] == 'E')
			board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_ON);
		if (pBuf[5] == 'T')
			board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_TOGGLE);
	}


	if (pBuf[4] == '2'){
		if (pBuf[5] == 'A')
			board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_OFF);
		if (pBuf[5] == 'E')
			board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_ON);
		if (pBuf[5] == 'T')
			board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_TOGGLE);
	}

	uart1_ringBuffer_envDatos(pBuf,indexRx);
}

static void accionesSW (uint8_t *pBuf){
	char valor_SW;
	uint8_t buffer[10];


	if (pBuf[4] == '1'){
		 if (board_getSw(1) )
			 valor_SW= 'P';
		 else
			 valor_SW= 'N';

	}

	if (pBuf[4] == '3'){
		 if (board_getSw(0) )
			 valor_SW= 'P';
		 else
			 valor_SW= 'N';
	}


	 buffer[0]= pBuf[0];
	 buffer[1]= pBuf[1];
	 buffer[2]= pBuf[2];
	 buffer[3]= pBuf[3];
	 buffer[4]= pBuf[4];
	 buffer[5]= valor_SW;
	 buffer[6]= pBuf[5];

	 uart1_ringBuffer_envDatos( buffer, 7);
}

static void accionesAccel (uint8_t *pBuf){
	uint8_t buffer[10];

	Lectura_actual =  sqrt( mma8451_getAcCuad()  );
	//Lectura_actual = mma8451_getAcY();

	if( Lectura_actual < 100){
		sprintf ( &buffer[6], "%2d" ,Lectura_actual );
		buffer[5] = '0';
	}
	else
		sprintf ( &buffer[5], "%3d" ,Lectura_actual );


	 buffer[0]= pBuf[0];
	 buffer[1]= pBuf[1];
	 buffer[2]= pBuf[2];
	 buffer[3]= pBuf[3];
	 buffer[4]= pBuf[4];

	 buffer[8]= pBuf[5];

	 uart1_ringBuffer_envDatos( buffer, 9);
}

/*==================[external functions definition]==========================*/

int main(void)
{
    int32_t ret;
    uint8_t buffer[20];

	BOARD_BootClockRUN();

	// Se inicializan funciones de la placa
	board_init();

	/* =========== I2C =================== */
	SD2_I2C_init();
	/* =========== MMA8451 ================ */
	mma8451_init();//Empieza en modo DRDY  Prioridad mma=2


	/* =========== Ring Buffer-UART0 =================== */
	//uart_ringBuffer_init();

	/* =========== Ring Buffer-UART1 =================== */
	uart1_rs485_ringbuffer_init();

	//probar si puedo usar ambasr uart a la vez

	/* =========== Systick =================== */
	SysTick_Init();


    while(1)
    {
    	mefRecDatos();
    }
}

void SysTick_Init(void){
	SysTick_Config(SystemCoreClock / 1000U);
	NVIC_SetPriority(SysTick_IRQn,1);
}

void SysTick_Handler(void){
    if (timeDown1ms)
    	timeDown1ms--;

    key_periodicTask1ms();
}



