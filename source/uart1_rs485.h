
/*==================[inclusions]=============================================*/
#include "stdint.h"
#include "stdbool.h"

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions definition]==========================*/

static void rs485_RE(bool est);
static void rs485_DE(bool est);
void uart1_rs485_ringbuffer_init(void);
void* rs485_Get_pRingBufferRx(void);
void* rs485_Get_pRingBufferTx(void);
void board_rs485_sendByte(uint8_t dato);
bool board_rs485_isDataAvailable(void);
uint8_t board_rs485_readByte(void);
int32_t uart1_ringBuffer_recDatos(uint8_t *pBuf, int32_t size);
int32_t uart1_ringBuffer_envDatos(uint8_t *pBuf, int32_t size);
